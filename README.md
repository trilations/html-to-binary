# Html to Binary

<!-- TODO: Add description -->

## Installation

Add it to your `composer.json`:
```json
{
    "repositories": [
        {
        "type": "vcs",
        "url":  "https://gitlab.com/trilations/html-to-binary.git"
        },
    ],
    "require": {
        "trilations/html-to-binary": "^2.4.0"
    }
}
```

And run composer update:
```sh
composer update
```

## Usage

<!-- TODO: Add usage -->

## Contributing

### Installation

```sh
# make sure you have php 7.4 or higher
php -v

# make sure you have composer installed
composer -v

# clone the repository
git clone git@gitlab.com:trilations/html-to-binary.git

# cd into the directory
cd html-to-binary

# install dependencies
composer install
```

## Coding Standards

Check and fix for violations against the coding standard
```sh
# run phpcs on the code (checks violations)
./vendor/bin/phpcs

# run phpcbf (fixes violations)
./vendor/bin/phpcbf

# add all warnings to the output
./vendor/bin/phpcs --warning-severity=1
```

Additionally, you should regularly check for any incompatibilies with our supported PHP versions:
```sh
# Runs the sniffer script as defined in composer.json
composer sniffer:php7.4
composer sniffer:php8.0
composer sniffer:php8.1

# Any other version can be checked like this
composer sniffer:php 7.1
```

### Tests

#### PHPUnit

```sh
# running all tests
./vendor/bin/phpunit

# checking the code coverage (requires Xdebug)
./vendor/bin/phpunit --coverage-text
./vendor/bin/phpunit --coverage-html coverage.html # generate html report

# you might need to prefix these commands with XDEBUG_MODE=coverage
XDEBUG_MODE=coverage ./vendor/bin/phpunit --coverage-text
```
