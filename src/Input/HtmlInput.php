<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Input;

use Trilations\HtmlToBinary\Interfaces\InputInterface;

/**
 * Class HtmlInput
 * @package Trilations\HtmlToBinary\Input
 */
class HtmlInput implements InputInterface
{
    /**
     * @var string
     */
    private $html;

    /**
     * @var bool
     */
    private $sanitized;

    /**
     * HtmlInput constructor.
     * @param string $html
     */
    public function __construct(string $html)
    {
        $this->html = $html;
        $this->sanitized = false;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return ['Content-Type' => 'text/html'];
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return [];
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->sanitize($this->html);
    }

    /**
     * @param string $html
     * @return string
     */
    private function sanitize(string $html): string
    {
        if (!$this->sanitized) {
            $this->html = $this->replaceLocalhost($html);
            $this->sanitized = true;
        }
        return $this->html;
    }

    /**
     * @param string $html
     * @return string
     */
    private function replaceLocalhost(string $html): string
    {
        if (!array_key_exists('REMOTE_ADDR', $_SERVER) || $_SERVER['REMOTE_ADDR'] == "localhost") {
            return $html;
        }
        return preg_replace("/(href|src)=(\"|')(https?):\/\/localhost/", "$1=$2$3://{$_SERVER['REMOTE_ADDR']}", $html);
    }
}
