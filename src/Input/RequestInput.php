<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Input;

use Psr\Http\Message\RequestInterface;
use Trilations\HtmlToBinary\Interfaces\InputInterface;

/**
 * Class RequestInput
 * @package Trilations\HtmlToBinary\Input
 */
class RequestInput implements InputInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * RequestInput constructor.
     * @param RequestInterface $request
     */
    public function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return ['Content-Type' => 'application/json'];
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return [];
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        $content = [];
        $content["url"] = $this->request->getUri();
        $content["headers"] = $this->request->getHeaders();
        $content["method"] = $this->request->getMethod();
        return json_encode($content);
    }
}
