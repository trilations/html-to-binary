<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Interfaces;

/**
 * Interface InputInterface
 * @package Trilations\HtmlToBinary\Interfaces
 */
interface InputInterface
{
    /**
     * @return array
     */
    public function getHeaders(): array;

    /**
     * @return array
     */
    public function getParameters(): array;

    /**
     * @return string
     */
    public function getBody(): string;
}
