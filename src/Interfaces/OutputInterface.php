<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Interfaces;

/**
 * Interface RequestInterface
 * @package Libraries\WebToImage\Interfaces
 */
interface OutputInterface
{
    /**
     * @return array
     */
    public function getHeaders(): array;

    /**
     * @return array
     */
    public function getParameters(): array;
}
