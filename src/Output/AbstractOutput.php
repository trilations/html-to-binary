<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Output;

use Trilations\HtmlToBinary\Interfaces\OutputInterface;

/**
 * Class AbstractRequest
 * @package Libraries\WebToImage\Request
 */
abstract class AbstractOutput implements OutputInterface
{
    /**
     * @var array|string[]
     */
    private $headers;

    /**
     * @var array|string[]
     */
    private $parameters;

    /**
     * AbstractRequest constructor.
     */
    public function __construct()
    {
        $this->headers = [];
        $this->parameters = [];
    }

    /**
     * @return array|string[][]
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param string      $name
     * @param string|null $value
     */
    protected function setHeader(string $name, string $value = null): void
    {
        $this->headers[$name] = $value;
    }

    /**
     * @return array|string[][]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    protected function setParameter(string $name, string $value): void
    {
        $this->parameters[$name] = $value;
    }
}
