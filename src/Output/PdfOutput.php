<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Output;

/**
 * Class PdfRequest
 * @package Libraries\WebToImage\Request
 */
class PdfOutput extends AbstractOutput
{
    public function __construct()
    {
        parent::__construct();
        $this->setHeader("Accept", "application/pdf");
    }

    /**
     * @param float $scale
     */
    public function setScale(float $scale): void
    {
        // limit the scale to 0.1 and 2.0
        // > https://pptr.dev/#?product=Puppeteer&version=v2.1.1&show=api-pagepdfoptions
        $scale = max(0.1, min(2.0, $scale));
        $this->setParameter('scale', (string) $scale);
    }
}
