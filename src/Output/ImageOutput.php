<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Output;

/**
 * Class Image
 * @package Libraries\WebToImage\Request
 */
class ImageOutput extends AbstractOutput
{
    public const TYPE_PNG = "png";
    public const TYPE_JPEG = "jpeg";

    private const DEFAULT_WIDTH = 1920;
    private const DEFAULT_HEIGHT = 1080;

    /**
     * @var string
     */
    private $type;

    /**
     * @var bool
     */
    private $fullPage;

    /**
     * @var int
     */
    private $width;

    /**
     * @var int
     */
    private $height;

    /**
     * ImageFormat constructor.
     * @param string $type
     */
    public function __construct(string $type = self::TYPE_PNG)
    {
        parent::__construct();
        $this->type = $type;
        $this->setHeader("Accept", "image/${type}");
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        $parameters = parent::getParameters();
        $parameters["fullPage"] = $this->isFullPage() ? "1" : "0";
        $parameters["width"] = (string) $this->getWidth();
        $parameters["height"] = (string) $this->getHeight();
        return $parameters;
    }

    /**
     * @param bool $fullPage
     */
    public function setFullPage(bool $fullPage): void
    {
        $this->fullPage = $fullPage;
    }

    /**
     * @return bool
     */
    public function isFullPage(): bool
    {
        return $this->fullPage ?? true;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width ?? self::DEFAULT_WIDTH;
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width): void
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height ?? self::DEFAULT_HEIGHT;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height): void
    {
        $this->height = $height;
    }
}
