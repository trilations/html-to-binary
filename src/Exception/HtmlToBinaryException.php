<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Exception;

use Exception;

/**
 * Class HtmlToBinaryException
 * @package Trilations\HtmlToBinary\Exception
 */
class HtmlToBinaryException extends Exception
{
}
