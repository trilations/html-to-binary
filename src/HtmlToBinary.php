<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary;

use Exception;
use Trilations\HtmlToBinary\Exception\HtmlToBinaryException;
use Trilations\HtmlToBinary\Input\HtmlInput;
use Trilations\HtmlToBinary\Input\RequestInput;
use Trilations\HtmlToBinary\Interfaces\InputInterface;
use Trilations\HtmlToBinary\Output\ImageOutput;
use Trilations\HtmlToBinary\Output\PdfOutput;
use Trilations\HtmlToBinary\Interfaces\OutputInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Class WebToImage
 * @package Libraries\WebToImage
 */
class HtmlToBinary
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var RequestFactoryInterface
     */
    private $requestFactory;

    /**
     * @var StreamFactoryInterface
     */
    private $streamFactory;

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * WebToImage constructor.
     * @param string                  $host
     * @param RequestFactoryInterface $requestFactory
     * @param StreamFactoryInterface  $streamFactory
     * @param ClientInterface         $httpClient
     */
    public function __construct(
        string $host,
        RequestFactoryInterface $requestFactory,
        StreamFactoryInterface $streamFactory,
        ClientInterface $httpClient
    ) {
        $this->host = $host;
        $this->requestFactory = $requestFactory;
        $this->httpClient = $httpClient;
        $this->streamFactory = $streamFactory;
    }

    /**
     * @param string          $contentType
     * @param string          $body
     * @param OutputInterface $output
     * @return StreamInterface
     * @throws HtmlToBinaryException
     */
    public function get(InputInterface $input, OutputInterface $output): StreamInterface
    {
        try {
            // Create URI
            $params = [];
            foreach ([$output->getParameters(), $input->getParameters()] as $parameters) {
                foreach ($parameters as $parameter => $value) {
                    $params[] = sprintf("%s=%s", urlencode($parameter), urlencode($value));
                }
            }

            // Create Request
            $request = $this->requestFactory->createRequest("POST", $this->host . "?" . implode("&", $params));

            // Add headers
            foreach ([$output->getHeaders(), $input->getHeaders()] as $headers) {
                foreach ($headers as $header => $value) {
                    $request = $request->withHeader($header, $value);
                }
            }

            // Add body
            $stream = $this->streamFactory->createStream($input->getBody());
            $request = $request->withBody($stream);

            try {
                $response = $this->httpClient->sendRequest($request);
            } catch (Exception $ex) {
                $msg = sprintf("Request failed to '%s' : %s", $request->getUri(), $ex->getMessage());
                throw new HtmlToBinaryException($msg, $ex->getCode(), $ex);
            }

            if ($response->getStatusCode() >= 400) {
                $error = $response->getReasonPhrase() ?? "Web request faild with code {$response->getStatusCode()}";
                throw new HtmlToBinaryException($error);
            }

            return $response->getBody();
        } catch (Exception $ex) {
            if (!$ex instanceof HtmlToBinaryException) {
                $ex = new HtmlToBinaryException($ex->getMessage(), $ex->getCode(), $ex);
            }
            throw $ex;
        }
    }

    /**
     * @param string $type
     * @return ImageOutput
     */
    public function createImageOutput(string $type = ImageOutput::TYPE_PNG): ImageOutput
    {
        return new ImageOutput($type);
    }

    /**
     * @return PdfOutput
     */
    public function createPdfOutput(): PdfOutput
    {
        return new PdfOutput();
    }

    /**
     * @param string $html
     * @return HtmlInput
     */
    public function createHtmlInput(string $html): HtmlInput
    {
        return new HtmlInput($html);
    }

    /**
     * @param RequestInterface $request
     * @return RequestInput
     */
    public function createRequestInput(RequestInterface $request): RequestInput
    {
        return new RequestInput($request);
    }
}
