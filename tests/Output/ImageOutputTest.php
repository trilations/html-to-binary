<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Tests\Output;

use PHPUnit\Framework\TestCase;
use Trilations\HtmlToBinary\Output\ImageOutput;

/**
 * Class ImageFormatTest
 * @package Trilations\HtmlToBinary\Tests\\Format
 */
class ImageOutputTest extends TestCase
{
    public function testImageMustHaveAcceptHeader()
    {
        $format = new ImageOutput();
        $headers = $format->getHeaders();
        $this->assertArrayHasKey("Accept", $headers);

        $format = new ImageOutput("png");
        $headers = $format->getHeaders();
        $this->assertEquals("image/png", $headers["Accept"]);

        $format = new ImageOutput("jpeg");
        $headers = $format->getHeaders();
        $this->assertEquals("image/jpeg", $headers["Accept"]);
    }

    public function testImageQueryParameters()
    {
        $format = new ImageOutput();

        // set default values
        $defaultParameters = $format->getParameters();
        $this->assertArrayHasKey("fullPage", $defaultParameters);
        $this->assertArrayHasKey("width", $defaultParameters);
        $this->assertArrayHasKey("height", $defaultParameters);

        $this->assertEquals($defaultParameters["fullPage"], "1");
        $this->assertIsNumeric($defaultParameters["width"]);
        $this->assertIsNumeric($defaultParameters["height"]);
    }

    public function testImageFormatDefaultValues()
    {
        $format = new ImageOutput();
        $this->assertTrue($format->isFullPage());
        $this->assertIsNumeric($format->getWidth());
        $this->assertIsNumeric($format->getHeight());
    }

    public function testOverridingDefaultValues()
    {
        $format = new ImageOutput();

        $format->setFullPage(false);
        $format->setWidth(100);
        $format->setHeight(200);

        $this->assertFalse($format->isFullPage());
        $this->assertEquals(100, $format->getWidth());
        $this->assertEquals(200, $format->getHeight());
    }
}
