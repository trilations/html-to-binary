<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Tests\Output;

use PHPUnit\Framework\TestCase;
use Trilations\HtmlToBinary\Output\PdfOutput;

/**
 * Class PdfFormatTest
 * @package Trilations\HtmlToBinary\Tests\\Format
 */
class PdfOutputTest extends TestCase
{
    public function testPdfMustHaveAcceptHeader()
    {
        $format = new PdfOutput();
        $headers = $format->getHeaders();
        $this->assertArrayHasKey("Accept", $headers);
        $this->assertEquals("application/pdf", $headers["Accept"]);
    }

    public function testPdfScale()
    {
        $format = new PdfOutput();

        $this->assertArrayNotHasKey('scale', $format->getParameters());

        $format->setScale(1);
        $this->assertArrayHasKey('scale', $format->getParameters());
        $this->assertEquals("1", $format->getParameters()["scale"]);

        $format->setScale(0.99);
        $this->assertArrayHasKey('scale', $format->getParameters());
        $this->assertEquals("0.99", $format->getParameters()["scale"]);

        // test minimum scale
        $format->setScale(0);
        $this->assertArrayHasKey('scale', $format->getParameters());
        $this->assertEquals("0.1", $format->getParameters()["scale"]);

        // test maximum scale
        $format->setScale(4);
        $this->assertArrayHasKey('scale', $format->getParameters());
        $this->assertEquals("2", $format->getParameters()["scale"]);
    }
}
