<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Tests;

use Trilations\HtmlToBinary\Exception\HtmlToBinaryException;
use Trilations\HtmlToBinary\Input\HtmlInput;
use Trilations\HtmlToBinary\Input\RequestInput;
use Trilations\HtmlToBinary\Interfaces\InputInterface;
use Trilations\HtmlToBinary\Interfaces\OutputInterface;
use Trilations\HtmlToBinary\Output\ImageOutput;
use Trilations\HtmlToBinary\Output\PdfOutput;
use Trilations\HtmlToBinary\HtmlToBinary;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Class WebToImageTest
 * @package Trilations\HtmlToBinary\Tests\
 */
class HtmlToBinaryTest extends TestCase
{
    /**
     * @var MockObject|ClientInterface
     */
    private $client;

    /**
     * @var HtmlToBinary
     */
    private $htmlToBinary;

    /**
     * @var MockObject|RequestFactoryInterface
     */
    private $requestFactory;

    /**
     * @var MockObject|RequestInterface
     */
    private $request;

    /**
     * @var MockObject|StreamFactoryInterface
     */
    private $streamFactory;

    /**
     * @var MockObject|StreamInterface
     */
    private $responseStream;

    /**
     * @var MockObject|ResponseInterface
     */
    private $response;

    /**
     * @var MockObject|InputInterface
     */
    private $input;

    /**
     * @var MockObject|OutputInterface
     */
    private $output;

    protected function setUp(): void
    {
        parent::setUp();

        $this->input = $this->createMock(InputInterface::class);
        $this->output = $this->createMock(OutputInterface::class);

        $this->responseStream = $this->createMock(StreamInterface::class);

        $this->requestFactory = $this->createMock(RequestFactoryInterface::class);
        $this->request = $this->createMock(RequestInterface::class);
        $this->requestFactory
            ->expects($this->once())
            ->method("createRequest")
            ->willReturn($this->request);
        $this->request
            ->expects($this->once())
            ->method("withBody")
            ->willReturn($this->request);

        $this->client = $this->createMock(ClientInterface::class);

        $this->response = $this->createMock(ResponseInterface::class);
        $this->response->expects($this->any())
            ->method("getBody")
            ->willReturn($this->responseStream);

        $this->client
            ->expects($this->once())
            ->method("sendRequest")
            ->willReturn($this->response);

        $this->streamFactory = $this->createMock(StreamFactoryInterface::class);
        $this->htmlToBinary = new HtmlToBinary("localhost", $this->requestFactory, $this->streamFactory, $this->client);
    }

    public function testInputBodyIsPassedToStream()
    {
        $this->input
            ->expects($this->any())
            ->method("getBody")
            ->willReturn("<input />");

        $this->streamFactory
            ->expects($this->once())
            ->method("createStream")
            ->with("<input />");

        $this->htmlToBinary->get($this->input, $this->output);
    }

    public function testParametersArePassedToRequest()
    {
        $this->input
            ->expects($this->once())
            ->method("getParameters")
            ->willReturn(["inputParam1" => "inputValue1"]);

        $this->output
            ->expects($this->once())
            ->method("getParameters")
            ->willReturn(["outputParam1" => "outputValue1"]);

        $this->requestFactory
            ->expects($this->once())
            ->method("createRequest")
            ->with($this->anything(), "localhost?outputParam1=outputValue1&inputParam1=inputValue1");

        $this->htmlToBinary->get($this->input, $this->output);
    }

    public function testHeadersArePassedToRequest()
    {
        $this->input
            ->expects($this->once())
            ->method("getHeaders")
            ->willReturn(["inputHeader1" => "inputValue1"]);

        $this->output
            ->expects($this->once())
            ->method("getHeaders")
            ->willReturn(["outputHeader1" => "outputValue1"]);

        $this->request
            ->expects($this->exactly(2))
            ->method("withHeader")
            ->withConsecutive(
                [
                 "outputHeader1",
                 "outputValue1",
                ],
                [
                 "inputHeader1",
                 "inputValue1",
                ]
            )
            ->willReturn($this->request);

        $this->htmlToBinary->get($this->input, $this->output);
    }

    public function testFailedHttpRequest()
    {
        $this->expectException(HtmlToBinaryException::class);

        $this->response->expects($this->any())
            ->method("getStatusCode")
            ->willReturn(500);

        $this->htmlToBinary->get($this->input, $this->output);
    }

    public function testFactoryMethods()
    {
        $requestInput = $this->htmlToBinary->createRequestInput($this->createMock(RequestInterface::class));
        $this->assertInstanceOf(RequestInput::class, $requestInput);
        $this->assertInstanceOf(HtmlInput::class, $this->htmlToBinary->createHtmlInput("<div />"));
        $this->assertInstanceOf(PdfOutput::class, $this->htmlToBinary->createPdfOutput());
        $this->assertInstanceOf(ImageOutput::class, $this->htmlToBinary->createImageOutput());

        $this->htmlToBinary->get($this->input, $this->output);
    }
}
