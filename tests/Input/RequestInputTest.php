<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Tests\Input;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Trilations\HtmlToBinary\Input\RequestInput;

/**
 * Class RequestInputTest
 * @package Trilations\HtmlToBinary\Tests\Input
 */
class RequestInputTest extends TestCase
{
    /**
     * @var MockObject|RequestInterface
     */
    private $request;

    /**
     * @var RequestInput
     */
    private $input;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->request = $this->createMock(RequestInterface::class);
        $this->input = new RequestInput($this->request);
    }

    public function testBody()
    {
        $this->request
            ->expects($this->once())
            ->method("getHeaders")
            ->willReturn(["header1" => "headerValue"]);

        $this->request
            ->expects($this->once())
            ->method("getUri")
            ->willReturn("http://example.com");

        $this->request
            ->expects($this->once())
            ->method("getMethod")
            ->willReturn("METHOD");

        $actual = $this->input->getBody();
        $exptected = "{\"headers\":{\"header1\":\"headerValue\"},\"url\":\"http://example.com\",\"method\":\"METHOD\"}";

        $this->assertJsonStringEqualsJsonString($exptected, $actual);
    }

    public function testRequestInputMustHaveJsonBody()
    {
        $this->assertArrayHasKey("Content-Type", $this->input->getHeaders());
        $this->assertEquals("application/json", $this->input->getHeaders()["Content-Type"]);
    }

    public function testRequestInputHasNoParameters()
    {
        $this->assertEmpty($this->input->getParameters());
    }
}
