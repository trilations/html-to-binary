<?php

declare(strict_types=1);

namespace Trilations\HtmlToBinary\Tests\Input;

use PHPUnit\Framework\TestCase;
use Trilations\HtmlToBinary\Input\HtmlInput;

/**
 * Class HtmlInputTest
 * @package Trilations\HtmlToBinary\Tests\Input
 */
class HtmlInputTest extends TestCase
{
    /**
     * @var HtmlInput
     */
    private $input;

    protected function setUp(): void
    {
        parent::setUp();
        $this->input = new HtmlInput("<div />");
    }

    public function testParameters()
    {
        $this->assertEmpty($this->input->getParameters());
    }

    public function testHeaders()
    {
        $this->assertArrayHasKey("Content-Type", $this->input->getHeaders());
        $this->assertEquals("text/html", $this->input->getHeaders()["Content-Type"]);
    }

    public function testBody()
    {
        $this->assertEquals("<div />", $this->input->getBody());
    }

    public function testHtmlSanitizing()
    {
        $html = '<html lang="en"><header><link href="http://localhost"></header></html>';
        $input = new HtmlInput($html);
        $sanitized = $input->getBody();
        $this->assertStringContainsString("http://localhost", $sanitized);

        $_SERVER["REMOTE_ADDR"] = "192.168.0.1";
        $input = new HtmlInput($html);
        $sanitized = $input->getBody();
        $this->assertStringNotContainsString("http://localhost", $sanitized);
    }
}
