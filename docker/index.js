const express = require('express')
const puppeteer = require('puppeteer');

const hostname = '0.0.0.0';
const port = process.env.PORT || 3000;

const browserOptions = {
  headless: true,
  args: [
    '--no-sandbox',
    '--disable-setuid-sandbox',
    '--disable-dev-shm-usage' // https://github.com/puppeteer/puppeteer/blob/master/docs/troubleshooting.md#tips
  ]
};

// if we skip the puppeteer version of Chromium, use Chrome
if (process.env.PUPPETEER_SKIP_CHROMIUM_DOWNLOAD === "true") {
  browserOptions.executablePath = '/usr/bin/chromium-browser';
}

(async () => {
  // launch browser
  const browser = await puppeteer.launch(browserOptions);

  const app = express();
  app.use(express.text({ type: "html", limit: "30mb" }));
  app.use(express.json({ type: "json", limit: "30mb" }));

  app.post('/', (req, res) => handleRequest(browser, req, res));
  app.listen(port, () => console.log(`App listening on port ${port}!`));
})();

async function handleRequest(browser, request, response)
{
  const accept = request.get('Accept');
  if (!accept) {
    response.statusCode = 400;
    response.send('Missing Accept header');
    return;
  }

  const contentType = request.get('Content-Type');
  if (!contentType) {
    response.statusCode = 400;
    response.send('Missing Content-Type header');
    return;
  }

  const body = request.body;
  const page = await createPage(browser, request.query);

  // load page based on "Content-Type" header
  switch (contentType) {
    case 'application/json':
      await loadPageWithRequest(page, body);
      break;
    case 'text/html':
      await loadPageWithContent(page, body, request.connection.remoteAddress);
      break;
  }

  // get output based on "Accept" header
  let content = "";
  response.set('Content-Type', accept);
  switch (accept) {
    case 'application/pdf':
      content = await getPageAsPDF(page, request.query);
      break;
    case 'image/png':
    case 'image/jpeg':
      let options = request.query;
      options.type = accept.replace('image/', '');
      content = await getPageAsImage(page, options);
      break;
    default:
      response.statusCode = 406;
      response.set('Content-Type', 'text/text');
      content = `Unsupported Accept header: ${accept}`;
      break;
  }

  console.log("Sending response");
  response.send(content);

  page.close();

}

async function createPage(browser, options)
{
  let page = await browser.newPage();

  let viewPort = {
    width: parseInt(options.width || 1920, 10),
    height: parseInt(options.height || 1080, 10),
    deviceScaleFactor: parseFloat(options.deviceScaleFactor || 1.00),
    isMobile: !!(options.isMobile || false),
    hasTouch: !!(options.hasTouch || false),
    isLandscape: !!(options.isLandscape || false)
  };
  await page.setViewport(viewPort);

  page.on('error', (err) => {
    console.log('error: ', err)
  })
  page.on('pageerror', (err) => {
    console.log('pageerror: ', err)
  })

  return page;
}

async function loadPageWithContent(page, html, remoteAddress)
{
  await page.setContent(html);
  console.log("Loaded content");
  return page;
}

async function loadPageWithRequest(page, request)
{
  await page.setExtraHTTPHeaders(request.headers || {});
  await page.goto(request.url);
  console.log("Loaded url", request.url);
  return page;
}

async function getPageAsImage(page, options)
{
  let imageOptions = {
    type: options.type || 'png',
    fullPage: !!(options.fullPage || true)
  };

  // set image quality for JPEG
  if (options.type === "jpeg") {
    imageOptions.quality = Math.min(100, Math.max(0, parseInt(options.quality) || 85, 10));
  } else {
    delete imageOptions.quality;
  }

  console.log(`Export as ${imageOptions.type}`);
  return page.screenshot(imageOptions);
}

async function getPageAsPDF(page, options)
{
  console.log("Export as PDF");
  return page.pdf({
    format: options.format || "A4",
    printBackground: !!(options.printBackground || true),
    scale: parseFloat(options.scale || 1)
  });
}
